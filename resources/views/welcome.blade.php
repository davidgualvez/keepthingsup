<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Keep things Up</title>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="bg-indigo-900 ">
        <div class="container mx-auto "> 
            <div class="text-center py-4 lg:px-4">
                <div class="p-2 bg-indigo-800 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex" role="alert">
                    <span class="flex rounded-full bg-indigo-500 px-2 py-1 text-xs font-bold mr-3">H e y!</span>
                    <span class="font-semibold mr-2 text-left flex-auto">I am underconstruction</span>
                    <!-- <svg class="fill-current opacity-75 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.95 10.707l.707-.707L8 4.343 6.586 5.757 10.828 10l-4.242 4.243L8 15.657l4.95-4.95z"/></svg> -->
                </div>
            </div>

            <div class="flex h-screen">
                <div class="m-auto">
                    <p class="font-light text-6xl text-white font-thin heartBeat">David Gualvez</p>
                </div>
            </div>
        </div>
    </body>
</html>
